<?php
namespace Loudsrl\Connector;

/**
 *  WebTick Connector class
 *
 *  Questa classe è un connettore per utilizzare le API del servizio WebTick di CREA
 *
 *  @author Loud Srl
 */
class WebTick
{
    
    /**  @var string $method define the default method to http or overwrite to https for production */
    private $method = 'http';

    private $wsdl;
    
    private $options;
    
    public function __construct ($method = null) 
    {
        $this->wsdl = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'WSC_Webtic.xml';
        if ($method != null) {
            $this->method = $method;
        }
        $this->options = array(
            'uri'=> $this->method .'://services.webtic.it/services/WSC_Webtic.asmx/',
            'style'=>SOAP_RPC,
            'use'=>SOAP_ENCODED,
            'soap_version'=>SOAP_1_1,
            'cache_wsdl'=>WSDL_CACHE_NONE,
            'connection_timeout'=>15,
            'trace'=>true,
            'encoding'=>'UTF-8',
            'exceptions'=>true
        );
        ini_set('soap.wsdl_cache_enabled', 0);
        ini_set('soap.wsdl_cache_ttl', 900);
        ini_set('default_socket_timeout', 15);
    }

     /**
     * Get Fiscal Address
     *
     * Get the ip and port of the fiscal server
     *
     * @param number $placeId A number containing the id of the place. 
     *
     * @return array
     */
    public function getFiscalAddress($placeId) 
    {
        error_log("Dentro la funzione getFiscalAddress");
        $url = "https://services.webtic.it/services/WSC_Webtic.asmx/_getFiscalAddress?idcinema=". strval($placeId);
        return $this->curlCall($url);
    }

    /**
     * Check Fiscal Server
     *
     * Check if fiscal server is online and functioning
     *
     * @param string $ip The fiscal server ip. 
     * @param number $port The fiscal server port. 
     *
     * @return bool true or false
     */
    public function checkFiscalServer($ip, $port) {
        error_log("Dentro la funzione checkFiscalServer");
        $url = $this->method . "://services.webtic.it/services/WSC_Webtic.asmx/_checkFiscalServer?fiscal_address=".strval($ip)."&fiscal_port=". strval($port);
        return $this->curlCall($url)[0] == "-1" ? false : true;
    }

    /**
     * Get Places List
     *
     * Get the list of all places in WebTick service
     *
     *
     * @return array
     */

    public function getPlacesList()
    {
        error_log("Dentro la funzione getPlaces");
        $url = $this->method . "://services.webtic.it/services/WSC_Webtic.asmx/_getCinemaList";
        return $this->curlCall($url)["cinemas"];  
        // SOLO CINEMA
    }
    /**
     * Get Event List
     *
     * Get the list of all events of a determined place
     *
     * @param number $placeId A number containing the id of the place. 
     *
     * @return array
     */
    public function getEventList($placeId) 
    {
        error_log("Dentro la funzione getEventList");
        $url = $this->method . "://services.webtic.it/services/WSC_Webtic.asmx/_getEventList?idcinema=". strval($placeId);
        return $this->curlCall($url);
    } 

     /**
     * Get performance List
     *
     * Get the list of all performance of a determined place
     *
     * @param number $placeId A number containing the id of the place. 
     * @param number $eventId A number containing the id of the event. 
     *
     * @return array
     */
    public function getPerformanceList($placeId,$eventId) 
    {
        error_log("Dentro la funzione getEventList");
        $url = $this->method . "://services.webtic.it/services/WSC_Webtic.asmx/_getPerformanceList?idcinema=". strval($placeId)."&idevento=". strval($eventId);
        return $this->curlCall($url);
    } 

    /**
     * Get Full Performance List
     *
     * Get the Events and performances of a given place
     *
     * @param number $placeId A number containing the id of the place. 
     *
     * @return array
     */
    public function getFullPerformanceListDetail($placeId)
    {
        error_log("Dentro la funzione getFullPerformanceListDetail");
        $url = $this->method . "://services.webtic.it/services/WSC_Webtic.asmx/_getFullPerformanceListDetail?idcinema=". strval($placeId);
        // return $this->curlCall($url);
        $array = $this->curlCall($url);
        $arrayFiltrato = [];
        $index = 0;
        if (!empty($array["eventi"]["evento"])) {
            foreach ($array["eventi"]["evento"] as $evento) {
                $evento["performances"] = [];
                array_push($arrayFiltrato, $evento);
                foreach ($array["performances"]["performance"] as $performance) {
                    error_log($performance["idevento"]);
                    error_log($arrayFiltrato[$index]["idevento"]);
                    if ($performance['idevento'] == $arrayFiltrato[$index]["idevento"]) {
                        array_push($arrayFiltrato[$index]["performances"],$performance);
                    }
                }
                $index++;
            }
        }
        return $arrayFiltrato;
        
    }

    /**
     * Get Price List
     *
     * Returns ticket of type "INTERNET INTERO" and "INTERNET RIDOTTO"
     *
     * @param string $placeId The id of the place. 
     * @param string $idperformance The id of the performance. 
     * @param string $idtariffa The id of tariffa. 
     *
     * @return array return array filtered for tycket type
     */

    public function getPriceList($placeId,$idperformance,$idtariffa) 
    {
        error_log("Dentro la funzione getPriceList");
        $url = $this->method . "://services.webtic.it/services/WSC_Webtic.asmx/_getPriceListFull?idcinema=".strval($placeId)."&idperformance=".strval($idperformance)."&idtariffa=".strval($idtariffa)."&MODE=0";
        $arrayBiglietti =  $this->curlCall($url);
        $filteredArray = [];
        foreach($arrayBiglietti["prices"]["price"] as $v){
            if ($v["ticket_type_code"] == 1 or $v["ticket_type_code"] == 2 ) {
                array_push($filteredArray, $v);
            }
        }
        return $filteredArray;
    }
    
    /**
     * Get Best Seat List
     *
     * Returns a list of seats
     *
     * @param string $placeId The id of the place. 
     * @param string $idperformance The id of the performance. 
     * @param string $nPostiRichiesti Number of seats. 
     * @param string $odp ??. 
     * @param number $codRicerca ??. 
     * @param number $nListe ??. 
     *
     * @return array return array of best seats
     */

    public function getBestSeatList($placeId,$idperformance,$nPostiRichiesti,$odp,$codRicerca,$nListe) 
    {
        error_log("Dentro la funzione getBestSeatList");
        $url = $this->method . "://services.webtic.it/services/WSC_Webtic.asmx/_getBestSeatList?idcinema=".strval($placeId)."&idperformance=".strval($idperformance)."&nPostiRichiesti=".strval($nPostiRichiesti)."&ODP=".strval($odp)."&codice_ricerca=".strval($codRicerca)."&nListe=".strval($nListe);
        return $this->curlCall($url);
    }
    /**
     * Count Seat 
     *
     * Returns a list of seats
     *
     * @param string $placeId The id of the place. 
     * @param string $idperformance The id of the performance. 
     *
     * @return array return seats count
     */

    public function countSeats($placeId,$idperformance) 
    {
        error_log("Dentro la funzione countSeats");
        $url = $this->method . "://services.webtic.it/services/WSC_Webtic.asmx/_getOccupancy?idcinema=".strval($placeId)."&idperformance=".strval($idperformance);
        $result = $this->curlCall($url);
        if (isset($result["posti"]["posto"])) {
            $seatsRemaining = $result["capienza"] - count($result["posti"]["posto"]);
        } else {
            $seatsRemaining = $result["capienza"];
        }
        return $seatsRemaining;
    }

    /**
     * Set Blocco posti BS
     *
     * Finds automaticaly seats and block them
     *
     * @param number $placeId The id of the place. 
     * @param number $performanceId The id of the transaction. 
     * @param string $uid uid e.g. "PIPPO". 
     * @param string $sessionId Unique id. 
     * @param number $idBiglietto Id of the ticket you want to block. 
     * @param string $nPosti Number of seats. 
     * @param string $odp Sector e.g "PT". 
     *
     * @return array ??
     */

    public function setBloccaPostiBS($placeId,$performanceId,$uid,$sessionId,$idBiglietto,$nPosti,$odp) 
    {
        error_log("Dentro la funzione setBloccaPostiBS");
        try 
        {
            $soap = new \SoapClient($this->wsdl, $this->options);
            $data = $soap->setBloccaPostiBS(
                array(
                    'idcinema'=>$placeId, 
                    'idPerformance'=>$performanceId,
                    'uid'=>$uid,
                    'sSessionId'=> $sessionId,
                    'idbiglietto'=> $idBiglietto,
                    'nPostiRichiesti'=> $nPosti,
                    'ODP'=> $odp,
                    'codice_ricerca'=> 0,
                    'nMax'=> 100,
                    'pSala'=> 100,
                    'tLimite'=> 100
                )
            );
        }
            catch(Exception $e) {
            die($e->getMessage());
        }
        $array = json_decode(json_encode($data), true);
        return $array;
    }

    /**
     * Set Acquisto posti Track Id
     *
     * Finds automaticaly seats and buys them
     *
     * @param number $placeId The id of the place. 
     * @param number $performanceId The id of the transaction. 
     * @param string $sessionId Unique id. 
     * @param number $idTrans Id of the transaction. 
     * @param number $orderId Id of the bank transaction. 
     * @param string $hash Credit Card hash. 
     * @param string $name User name. 
     * @param string $email User email. 
     * @param array $seat_list Seat list array. 
     * @param array $fiscal_server Fiscal server array. 
     *
     * @return array ??
     */

    public function setAcquistoPostiTrackId($placeId,$performanceId,$sessionId,$idTrans,$orderId,$hash,$name,$email,$seat_list, $fiscal_server ) 
    {
        error_log("Dentro la funzione setAcquistoPostiTrackId");
        try 
        {
            $soap = new \SoapClient($this->wsdl, $this->options);
            $data = $soap->setAcquistoPostiTrackId(
                array(
                    'idcinema'=>$placeId, 
                    'idPerformance'=>$performanceId,
                    'sSessionId'=> $sessionId,
                    'iCodSistema'=> 100,
                    'idTrans'=> $idTrans,
                    'idOrdine'=> $orderId,
                    'sCodCC'=> $hash,
                    'sNominativo'=> $name,
                    'sEmail'=> $email,
                    'listaPosti' => $seat_list,
                    'fiscal_mode' => '1',
                    'fiscal_address'=>$fiscal_server['fiscal_address'],
                    'fiscal_port'=>$fiscal_server['fiscal_port'],
                    'session_enabled'=> 0,
                    'transaction_enabled'=> 1,
                    'web_box'=> 'BOX_ZERO',
                    'web_operator'=> 'USER_ZERO',
                    'trackid' => 500
                )
            );
        }
            catch(Exception $e) {
            die($e->getMessage());
        }
        $array = json_decode(json_encode($data), true);
        return $array;
    }
    /**
     * Set Sblocco posti 
     *
     * Unlock seats
     *
     * 
     * @param number $placeId The id of the place. 
     * @param number $performanceId The id of the transaction. 
     * @param string $sessionId Unique id. 
     * @param array $seat_list Seat list array. 
     *
     * @return array ??
     */

    public function setSbloccoPosti($placeId,$performanceId,$sessionId,$seat_list) 
    {
        error_log("Dentro la funzione setSbloccoPosti");
        try 
        {
            $soap = new \SoapClient($this->wsdl, $this->options);
            $data = $soap->setSbloccoPosti(
                array(
                    'idcinema'=>$placeId, 
                    'idPerformance'=>$performanceId,
                    'sSessionId'=> $sessionId,
                    'iCodSistema'=> 100,
                    'aiProgPosti' => $seat_list
                )
            );
        }
            catch(Exception $e) {
            die($e->getMessage());
        }
        $array = json_decode(json_encode($data), true);
        return $array;
    }

    /**
     * Set Be Transaction Printed
     *
     * Set the ticket(s) to be print at home
     *
     * @param string $placeId The id of the place. 
     * @param string $idtransaction The id of the transaction. 
     *
     * @return array ??
     */

    public function setBeTransactionPrinted($placeId,$idtransaction)
    {
        error_log("Dentro la funzione setBeTransactionPrinted");
        $url = "http://servicesticketone.webtic.it/services/WSC_Webtic.asmx/setBeTransactionPrinted?idcinema=".$placeId."&idtransaction=".$idtransaction;
        return $this->curlCall($url);
    }


    /**
     * Get Transaction Info
     *
     * Get the transaction info to generate the ticket
     *
     * @param string $placeId The id of the place. 
     * @param string $idtransaction The id of the transaction. 
     *
     * @return array ??
     */

    public function getTransactionInfo($placeId,$idtransaction)
    {
        error_log("Dentro la funzione setBeTransactionPrinted");
        $url = "http://servicesticketone.webtic.it/services/WSC_Webtic.asmx/_getTransactionInfo?idcinema=".$placeId."&idtransaction=".$idtransaction;
        return $this->curlCall($url);
    }

    private function curlCall($url) 
    {
        error_log("Url chiamato: ".$url);
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            $error = curl_error($ch);
            throw new Exception($error);
            // return 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        $xml = simplexml_load_string($result);
        $json = json_encode($xml);
        $arr = json_decode($json, true);

        return $arr;
    }
}
