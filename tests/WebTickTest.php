<?php

/**
 *  Corresponding Class to test WebTick class
 *
 *  @author Loud Srl
 */
class WebTickTest extends PHPUnit_Framework_TestCase
{
    
    /**
     *
     * Just check if the WebTick has no syntax error
     *
     */
    public function testIsThereAnySyntaxError()
    {
        $var = new Loudsrl\Connector\WebTick;
        $this->assertTrue(is_object($var));
        unset($var);
    }
}
