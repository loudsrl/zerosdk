CREA CONNECTOR
=========================
[![License](https://poser.pugx.org/pugx/badge-poser/license.svg)](https://packagist.org/packages/pugx/badge-poser)

This Composer library is an SDK to connect to Crea Webtic webservice trough php.

Features
--------

* PSR-4 autoloading compliant structure
* Unit-Testing with PHPUnit



I encourage that you put more information on this readme file instead of leaving it as is. See [http://www.darwinbiler.com/designing-and-making-the-readme-file-for-your-github-repository/](How to make a README file) for more info.
